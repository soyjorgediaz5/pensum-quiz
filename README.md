# Pensum-Quiz

> Educational project created at Universidad de Antioquia for the course Comunicaciones II.

> Challenge your friends with tests about Computer Science Engineering!

## How to run this app

1. Clone the repo - https://gitlab.com/soyjorgediaz5/pensum-quiz.git

2. Open `src/components/ChannelDetails` and `server.js` and edit with your  Pusher credentials which can be gotten from the [Pusher dashboard](https://pusher.com)

3. In the root of the project folder, run `npm i` to install all dependencies.

4. Then run `npm run dev` to get the app up and running.

>#### Game URL: https://pensum-quiz.firebaseapp.com/pusher/auth
